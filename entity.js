(function (engine) {
    'use strict';

    var Block = function (options) {
        var width = options.width;
        var height = options.height;

        // left-top coordinates of block
        var position = engine.Vector(options.x || 0, options.y || 0);
        // right-bottom coordinates of block
        var max_position = engine.Vector(position.x + width, position.y + height);

        var get_collision = function (block) {

            var collision = {
                top: false,
                bottom: false,
                left: false,
                right: false
            };

            if(!(this.max_position.x < block.position.x ||
                 block.max_position.x < this.position.x ||
                 this.max_position.y < block.position.y ||
                 block.max_position.y < this.position.y)) {

                if (this.position.x > block.position.x &&
                    this.max_position.x > block.max_position.x) {
                    collision.right = true;
                }

                if (this.position.x < block.position.x &&
                    this.max_position.x < block.max_position.x) {
                    collision.left = true;
                }

                if (this.position.y > block.position.y &&
                    this.max_position.y > block.max_position.y) {
                    collision.bottom = true;
                }

                if (this.position.y < block.position.y &&
                    this.max_position.y < block.max_position.y) {
                    collision.top = true;
                }
            }

            return collision;
        };

        return {
            position: position,
            max_position: max_position,
            width: width,
            height: height,

            get_collision: get_collision
        };
    };

    var Hero = function (options) {
        var animation = {
            'air-up-right': [{x: 2, y: 0}],
            'air-down-right': [{x: 3, y: 0}],
            'ground-move-right': [{x: 1, y: 2}, {x: 2, y: 2}],
            'ground-right': [{x: 0, y: 2}],

            'air-up-left': [{x: 2, y: 1}],
            'air-down-left': [{x: 3, y: 1}],
            'ground-move-left': [{x: 4, y: 2}, {x: 5, y: 2}],
            'ground-left': [{x: 3, y: 2}],

            'none': [{x: 0, y: 2}]
        };

        var canvas = options.canvas;
        var ctx = options.ctx;

        var sprite = engine.Sprite({
            url: 'img/sprites.png',
            width: 48,
            height: 48,
            frames: animation['none']
        });

        var block = Block({
            x: options.x || canvas.width / 2,
            y: options.y || canvas.height - sprite.height,
            width: 40,
            height: 48,
        });

        block.direction = 'none';
        block.speed_jump = options.speed_jump || 7;
        block.speed_v = engine.Vector(0, 0);
        block.acceleration_v = engine.Vector(0, 0.163);
        block.speed = options.speed || 5;

        var move_up = function () {
            if (block.position.y >= 0) {
                block.acceleration_v = block.acceleration_v.minus(engine.Vector(0, block.speed_jump));
            }
        };

        var move_down = function () {
            if (block.position.y <= canvas.height - block.height) {
                block.speed_v.y = block.speed;
            }
        };

        var move_left = function () {
            if (block.position.x >= 0) {
                block.speed_v.x = -block.speed;
            }
        };

        var move_right = function () {
            if (block.position.x <= canvas.width - block.width) {
                block.speed_v.x = block.speed;
            }
        };
        
        block.move = function() {
            var up = engine.input.is_down_once('up') || 
                     engine.input.is_down_once('w') || 
                     engine.input.is_down_once('space');

            var down = engine.input.is_down('down') || engine.input.is_down('s');
            var left = engine.input.is_down('left') || engine.input.is_down('a');
            var right = engine.input.is_down('right') || engine.input.is_down('d');

            var self = this;

            engine.controller.send_to_server({}, function (new_state) {

                /*
                *
                * this logic must be transfered to server !!
                *
                */
                

                var collision = self.check_collision(engine.map);

                sprite.clear(ctx, self.position);

                if(collision.top && (collision.left || collision.right)) {
                    self.speed_v.y = 0;/*self.speed_v.y > 0 ? 0 : self.speed_v.y;*/
                    self.acceleration_v.y = 0;//self.acceleration_v.y > 0 ? 0 : self.acceleration_v.y;

                    // correct block position
                    self.position.y = collision.top.position.y - self.height;
                    self.max_position.y = self.position.y + self.height;
                }
                else if(collision.bottom && (collision.left || collision.right)) {
                    self.speed_v.y = self.speed_v.y < 0 ? 0 : self.speed_v.y;
                    self.acceleration_v.y = self.acceleration_v.y < 0 ? 0 : self.acceleration_v.y;

                    // correct block position
                    self.position.y = collision.bottom.position.y + collision.bottom.height;
                    self.max_position.y = self.position.y + self.height;
                }
                else if(collision.top) {
                    self.speed_v.y = self.speed_v.y > 0 ? 0 : self.speed_v.y;
                    self.acceleration_v.y = self.acceleration_v.y > 0 ? 0 : self.acceleration_v.y;

                    // correct block position
                    self.position.y = collision.top.position.y - self.height;
                    self.max_position.y = self.position.y + self.height;
                }
                else if(collision.bottom) {
                    self.speed_v.y = self.speed_v.y < 0 ? 0 : self.speed_v.y;
                    self.acceleration_v.y = self.acceleration_v.y < 0 ? 0 : self.acceleration_v.y;
                    
                    // correct block position
                    self.position.y = collision.bottom.position.y + collision.bottom.height;
                    self.max_position.y = self.position.y + self.height;
                }
                else if (collision.left) {
                    self.speed_v.x = self.speed_v.x > 0 ? 0 : self.speed_v.x;
                    self.acceleration_v.x = self.acceleration_v.x > 0 ? 0 : self.acceleration_v.x;

                    // correct block position
                    self.position.x = collision.left.position.x - self.width;
                    self.max_position.x = self.position.x + self.width;
                }
                else if (collision.right) {
                    self.speed_v.x = self.speed_v.x < 0 ? 0 : self.speed_v.x;
                    self.acceleration_v.x = self.acceleration_v.x < 0 ? 0 : self.acceleration_v.x;

                    // corect block position
                    self.position.x = collision.right.position.x + collision.right.width;
                    self.max_position.x = self.position.x + self.width;
                }            

                // canvas border position handle
                if (self.max_position.y >= canvas.height) {
                    self.acceleration_v = engine.Vector(0, 0);
                    self.speed_v.y = 0;

                    self.position.y = canvas.height - self.height;
                    self.max_position.y = self.position.y + self.height;

                    if (up) {
                        move_up();
                    }
                }
                else if (!collision.top){
                    self.acceleration_v = engine.Vector(0, 0.16);
                }

                if (self.position.y <= 0) {
                    self.speed_v.y = 0;

                    self.position.y = 0;
                    self.max_position.y = self.position.y + self.height;
                }

                if (self.max_position.x >= canvas.width && self.speed_v.x > 0) {
                    self.speed_v.x = 0;

                    self.position.x = canvas.width - self.width;
                    self.max_position.x = self.position.x + self.width;
                }

                if ( self.position.x <= 0 && self.speed_v.x < 0) {
                    self.speed_v.x = 0;

                    self.position.x = 0;
                    self.max_position.x = self.position.x + self.width;
                }

                if (collision.top && up) {
                    move_up();
                }

                // if (down) {
                //     move_down();
                // }
                
                if (right) {
                    move_right();
                } else if (left) {
                    move_left();
                } else {
                    self.speed_v.x = 0;
                }

                self.speed_v = self.speed_v.plus(self.acceleration_v);
                self.position = self.position.plus(self.speed_v);
                self.max_position = self.max_position.plus(self.speed_v);
            });
        };

        block.check_collision = function (map) {
            var collision = {};
            for (var key in map) {
                var target = map[key];
                var res = this.get_collision(target);

                if (res.top) {
                    collision.top = target;
                }

                if (res.bottom) {
                    collision.bottom = target;   
                }

                if (res.left) {
                    collision.left = target;   
                }

                if (res.right) {
                    collision.right = target;
                }
            }

            return collision;
        }

        block.animate = function (dt) {
            var is_left = this.direction.indexOf('left') != -1;

            if (this.speed_v.y < 0) {

                if (this.speed_v.x < 0) {
                    this.direction = 'air-up-left';
                }
                else if (this.speed_v.x > 0) {
                    this.direction = 'air-up-right';
                }
                else {
                    this.direction = is_left ? 'air-up-left' : 'air-up-right';
                }

            } 
            else if (this.speed_v.y > 0) {

                if (this.speed_v.x < 0) {
                    this.direction = 'air-down-left';
                }
                else if (this.speed_v.x > 0) {
                    this.direction = 'air-down-right';
                }
                else {
                    this.direction = is_left ? 'air-down-left' : 'air-down-right';
                }
            } 
            else {

                if (this.speed_v.x < 0) {
                    this.direction = 'ground-move-left';
                }
                else if (this.speed_v.x > 0) {
                    this.direction = 'ground-move-right';
                }
                else {
                    this.direction = is_left ? 'ground-left' : 'ground-right';
                }
            }

            sprite.render(ctx, this.position, animation[this.direction], dt);
        };

        block.init = function() {
            sprite.init(ctx, this.position);
        };

        return block;
    };

    var Wall = function(options) {

        var brick = engine.Sprite({
            url: 'img/brick.png',
            width: 48,
            height: 48,
            frames: [{x:0, y: 0}]
        });

        var block = Block({
            x: options.x,
            y: options.y,
            width:  options.align === "horizontal" ? options.size * 30 : brick.width,
            height: options.align !== "horizontal" ? options.size * 30 : brick.height
        });

        block.size = options.size;
        block.align = options.align;

        block.paint = function(options) {
            if (this.align === "horizontal") {
                for (var i =0; i < this.size; i++) {
                    brick.render(options.ctx, {
                        x: this.position.x + i * 30, 
                        y: this.position.y
                    }, [{x:0, y: 0}], options.dt);
                }
            } else {
                for (var i =0; i < this.size; i++) {
                    brick.render(options.ctx, {
                        x: this.position.x, 
                        y: this.position.y + i * 30
                    }, [{x:0, y: 0}], options.dt);
                }
            }
        };

        return block;
    }

    engine.Block = Block;
    engine.Hero = Hero;
    engine.Wall = Wall;
})(engine);