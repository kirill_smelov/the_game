(function (engine) {
    'use strict';

    var lastTime;
    var logic_fps = 40;

    var animate_callback = function () {};
    var update_callback = function () {};

    // A cross-browser requestAnimationFrame
    // See https://hacks.mozilla.org/2011/08/animating-with-javascript-from-setinterval-to-requestanimationframe/
    var requestAnimFrame = (function() {
        return window.requestAnimationFrame       ||
            window.webkitRequestAnimationFrame ||
            window.mozRequestAnimationFrame    ||
            window.oRequestAnimationFrame      ||
            window.msRequestAnimationFrame     ||
            function(callback){
                window.setTimeout(callback, 1000 / 60);
            };
    })();

    var requestLogicFrame = (function () {
        return function(callback){
            window.setTimeout(callback, 1000 / logic_fps);
        };
    })();


    // animation loop
    var anim_loop = function () {
        var now = performance.now();
        var dt = (now - lastTime);

        animate(dt);

        lastTime = now;
        requestAnimFrame(anim_loop);
    };

    // logic loop
    var logic_loop = function () {
        update();
        requestLogicFrame(logic_loop);
    };

    var init = function () {
        lastTime = performance.now();
        anim_loop();
        logic_loop();
    };

    var animate = function (dt) {
        if (animate_callback instanceof Function) {
            animate_callback(dt);
        }
    };

    var update = function () {
        if (update_callback instanceof Function) {
            update_callback();
        }
    };

    engine.game = {
        init: init,
        on_update: function (callback) {
            update_callback = callback;
        },
        on_animate: function (callback) {
            animate_callback = callback;
        }
    };
})(engine);