(function (engine) {
    'use strict';

    var pressed_keys = {};
    var pressed_once_keys = {};

    var set_key = function (event, status) {
        var code = event.keyCode;
        var key;

        switch(code) {
            case 32:
                key = 'SPACE'; break;
            case 37:
                key = 'LEFT'; break;
            case 38:
                key = 'UP'; break;
            case 39:
                key = 'RIGHT'; break;
            case 40:
                key = 'DOWN'; break;
            default:
                // Convert ASCII codes to letters
                key = String.fromCharCode(code);
        }

        if (pressed_keys[key] !== status) {
            pressed_keys[key] = status;
            pressed_once_keys[key] = status;
        }
    };

    var is_down = function(key) {
        var upper_key = key.toUpperCase();
        return pressed_keys[key.toUpperCase()] || false;
    };

    var is_down_once = function(key) {
        var upper_key = key.toUpperCase();

        if (pressed_once_keys[upper_key]) {
            pressed_once_keys[upper_key] = false;
            return true;
        }
        else {
            return false;
        }
    };

    document.addEventListener('keydown', function(e) {
        set_key(e, true);
    });

    document.addEventListener('keyup', function(e) {
        set_key(e, false);
    });

    window.addEventListener('blur', function() {
        pressed_keys = {};
        pressed_once_keys = {};
    });

    engine.input = {
        is_down: is_down,
        is_down_once: is_down_once
    };
})(engine);