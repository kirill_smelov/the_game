
/*
* Sample of object to communicate with node.js 
*/

(function (engine) {
    'use strict';

    engine.controller = {
        
    	send_to_server: function (state, callback) {

    		window.setTimeout(function() {
    			var new_state = {};
    			if (callback instanceof Function) {
    				callback(new_state);
    			}
    		}, 200);

    	}

    };
})(engine);