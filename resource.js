(function(engine) {
    'use strict';

    var cache = {};
    var loading = [];
    var callbacks = [];

    // load an image url or an array of image urls
    var load = function (urls) {
        urls = urls instanceof Array ? urls : [urls];
        urls.forEach(function(url) {
            _load(url);
        });
        
        return {
            then: then
        };
    };

    var _load = function (url) {
        if(cache[url]) {
            return cache[url];
        }
        else {
            var img = new Image();
            img.onload = function() {
                cache[url] = img;
                
                if(is_ready()) {
                    while(callbacks.length) {
                        exec(callbacks.pop());
                    }
                }
            };
            cache[url] = false;
            img.src = url;
        }
    };

    // get image from cache
    var get = function (url) {
        return cache[url];
    };

    var is_ready = function () {
        var ready = true;
        for(var k in cache) {
            if(cache.hasOwnProperty(k) &&
               !cache[k]) {
                ready = false;
            }
        }
        return ready;
    };

    var then = function (func) {
        if(is_ready()) {
            exec(func);
        }
        else {
            callbacks.push(func);
        }
    };

    var exec = function (func) {
        if (func instanceof Function) {
            func();
        }
    };

    engine.resource = {
        load: load,
        get: get
    };
})(engine);