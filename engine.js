var engine = window.engine || {};
(function () {
    'use strict';

    var Vector = function(x, y) {
    	var plus = function(other) {
    		return Vector(this.x + other.x, this.y + other.y)
    	}

    	var minus = function(other) {
    		return Vector(this.x - other.x, this.y - other.y);
    	}
    	//Скалярное произведение
    	var dotProduct = function(other) {
    		return Vector(this.x * other.x, this.y * other.y)
    	}

    	var getLength = function() {
    		return Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2));
    	}

    	var normalize = function() {
    		var length = this.getLength();
    		return Vector(this.x / length, this.y / length);
    	}
    	
    	return {
    		x: x,
    		y: y,
    		plus: plus,
    		minus: minus,
    		dotProduct: dotProduct,
    		getLength: getLength,
    		normalize: normalize
    	};
    }

    engine.Vector = Vector;
})();

