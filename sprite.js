(function(engine) {
    'use strict';
    
    var Sprite = function (options) {

        var _last_x;
        var _last_y;
        var _frames = options.frames;
        var _last_frames = _frames;
        var _index = 0;
        var _sub_index = 0;

        var width = options.width;
        var height = options.height;
        var speed = options.speed || .7;
        var url = options.url;

        // render sprite with animation
        var render = function (ctx, pos, frames, dt) {
            frames = frames || _frames;

            var frame = animate(frames, dt);
            var frame_pos_x = frame.x * this.width;
            var frame_pos_y = frame.y * this.height;

            ctx.drawImage(engine.resource.get(this.url),
                          frame_pos_x, frame_pos_y,
                          this.width, this.height,
                          // ~~pos.x, ~~pos.y,
                          pos.x, pos.y,
                          this.width, this.height);
        };

        // init sprite in canvas
        var init = function (ctx, pos) {
            var frame = _frames[0];
            var frame_pos_x = frame.x * this.width;
            var frame_pos_y = frame.y * this.height;

            ctx.drawImage(engine.resource.get(this.url),
                          frame_pos_x, frame_pos_y,
                          this.width, this.height,
                          // ~~pos.x, ~~pos.y,
                          pos.x, pos.y,
                          this.width, this.height);
        };

        // clear prev area
        var clear = function (ctx, pos) {
            ctx.clearRect(pos.x, pos.y, this.width, this.height);
        };

        var animate = function (frames, dt) {

            if(_last_frames != frames) {
                // if we render new frames set, 
                // then we must reset _index and _last_frames storage
                _index = 0;
                _last_frames = frames;
            }

            var delay = (dt * speed);
            if (++_sub_index >= delay) {
                _index = (_index + 1) % frames.length;
                _sub_index = 0;
            }
            
            return frames[_index];
        };

        return {
            // fields
            width: width,
            height: height,
            url: url,
            speed: speed,

            // methods
            init: init,
            render: render,
            clear: clear
        };
    };

    var getRandomMap = function(options) {
        var walls = new Array();
        
        for (var i = 0; i < options.num; i++) {
            var size=Math.floor(Math.random()*options.max_size);
            var x = Math.floor(Math.random()*options.max_x);
            var y = Math.floor(Math.random()*options.max_y);
            var align = Boolean(Math.floor(Math.random()*2)) ? 'vertical' : 'horizontal';

            walls.push({size: size,x: x, y: y, align: align});
        }
        return walls;
    }

    engine.getRandomMap = getRandomMap;
    engine.Sprite = Sprite;
})(engine);