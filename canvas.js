$(function() {
    'use strict';

    var canvas = document.createElement('canvas');
    var ctx = canvas.getContext('2d');
    var background;

    var player;

    var levels = new Array([
            {size: 16,x: 500, y: 150, align: "vertical"},
            {size: 8,x: 385, y: 75, align: "horizontal"},
            {size: 8,x: 50, y: 590, align: "horizontal"},
            {size: 6,x: 300, y: 450, align: "horizontal"},
            {size: 4,x: 50, y: 300, align: "horizontal"},
            {size: 4,x: 300, y: 200, align: "horizontal"},

            {size: 8,x: 700, y: 590, align: "horizontal"},
            {size: 6,x: 550, y: 450, align: "horizontal"},
            {size: 4,x: 800, y: 300, align: "horizontal"},
            {size: 4,x: 600, y: 200, align: "horizontal"}
        ],

        [
            {size: 20,x: 750, y: 150, align: "vertical"},
            {size: 2,x: 900, y: 100, align: "horizontal"},
            {size: 4,x: 600, y: 200, align: "horizontal"},
            {size: 4,x: 400, y: 300, align: "horizontal"},
            {size: 6,x: 300, y: 300, align: "vertical"},
            {size: 8,x: 50, y: 590, align: "horizontal"},
            {size: 4,x: 50, y: 400, align: "horizontal"},
            {size: 1,x: 200, y: 500, align: "horizontal"},
        ],

        [
            {size: 10,x: 850, y: 400, align: "vertical"},
            {size: 10,x: 950, y: 400, align: "vertical"},
            {size: 27,x: 40, y: 600, align: "horizontal"},
            {size: 10,x: 40, y: 480, align: "horizontal"},
            {size: 7,x: 400, y: 388, align: "vertical"},
            {size: 1,x: 400, y: 230, align: "vertical"},
            {size: 2,x: 300, y: 300, align: "horizontal"},
            {size: 1,x: 530, y: 300, align: "vertical"},
            {size: 2,x: 200, y: 200, align: "horizontal"},
            {size: 3,x: 375, y: 140, align: "horizontal"},
            {size: 6,x: 620, y: 120, align: "horizontal"},
        ]
        );

    var walls = levels[Math.floor(Math.random()*levels.length)];
    canvas.id     = "tutorial";
    canvas.width  = 1000;
    canvas.height = 700;
    document.body.appendChild(canvas);

    /*var walls = engine.getRandomMap({
        num: 20,
        max_size: 10,
        max_x: canvas.width,
        max_y: canvas.height
    });*/

    var map = new Array();

    // init game after loading all need resources
    engine.resource.load([
        'img/sprites.png',
        'img/terrain.png',
        'img/brick.png'
    ])
    .then(function() {

        player = engine.Hero({
            canvas: canvas,
            ctx: ctx
        });

        player.init();
        
        for (var key in walls) {
            map.push(engine.Wall(walls[key]));
        }

        engine.map = map;

        for (var key in map) {
            map[key].paint({
                dt: 0,
                ctx: ctx
            });
        }

        engine.game.init();   
    });

    engine.game.on_update(function () {
        player.move();
    });

    engine.game.on_animate(function (dt) {

        // render player
        player.animate(dt);

        // render walls
        for (var key in map) {
            map[key].paint({
                dt: dt,
                ctx: ctx
            });
        }

    });
    
});